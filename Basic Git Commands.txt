GIT VCS

What is Git VCS?
    - GIT VCS is an open source version contorl system. It allows us to track and update our files with only the changes made to it.

 GitLab vs GitHub
    - Both GitLab and GitHub are similar competing cloud services that store and manage our online repositories.

Two Types of Repositories:
    - Local Repository
        - folders that use git technology. Therefore, it allows us to track changes made in the files with with the folder

        - These changes can be uploaded and update the files in our remote repository

    - Remote Repository
        - these are the folders that use git technology but located in the internet or in cloud services such as GitLab and GitHub

        Local Machine to Remote Repo

        Local Machine-<SSH Key>-GitLab/GitHub

What is an SSH Key?
        - SSH or Secure Shell key is a tool used to authenticate the uploading or of other tasks when manipulating or using Git repository.
        
        - it allows us to push/upload changes to our repositories without the need of password

        Basic Git Commands:

        git init
            - it allows us to initialize a foder as a local remote repository

        git status
            -it allows us to display or verify files that are ready to be added and committed

        git add .
            -it allows us to track all the changes made and prepare these files as a new version to be uploaded

        git commit -m"<commitMessage>""
            -it allows us to create a new commit or version of our file to be pushed into our remote repositories.

        git remote add <aliasOfRemote> <urlOfRemote>
            - it allows us to add/connect a remote repository to our local repository

        git remote -v
            - it allows us to view the remote repositories connected to our local repository

        git push <alias> master/(main)
            - it allows us to push our updates/changes/version commit into our remote repository

        git log 
            - show the version history of the repo

Git Config
            - these commands allow us to have git recognize the person trying to push into an online/remote repository

        git config --global user.email "emailUsedInGitLab"
            - configure the email used to push into the remote repository

        git config --global user.name "userNameUsedInGitLab"
            - configure the name/username of the user trying to push into GitLab

/*Mini Activity*/
     
